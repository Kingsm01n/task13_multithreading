package Fibonacci;

import MVCpattern.View;

import java.util.concurrent.ThreadFactory;

public class Fibonacci extends Thread{
    int n;
    Object o = new Object();
    public Fibonacci(int n){
        this.n = n;
    }
    int n1 = 1;
    int n2 = 1;
    int n3 = 0;
    @Override
    public void run() {
        synchronized (o) {
            View.display(n1);
            View.display(n2);
            while (n3 < n) {
                n3 = n1 + n2;
                View.display(n3);
                n1 = n2;
                n2 = n3;
            }
        }
    }
}
