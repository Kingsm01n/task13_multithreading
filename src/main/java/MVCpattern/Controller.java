package MVCpattern;

import Fibonacci.Fibonacci;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Controller {
    private static final Object o = new Object();
    public void pingPong() {
        Thread myTread1 = new Thread(() -> {
            synchronized (o) {
                for (int i = 1; i < 10; i++) {
                    try {
                        o.wait();
                    } catch (InterruptedException e) {
                    }
                    View.display("ping");
                    o.notify();
                }
            }
        });
        Thread myTread2 = new Thread(() -> {
            synchronized (o) {
                for (int i = 1; i < 10; i++) {
                    o.notify();
                    try {
                        o.wait();
                    } catch (InterruptedException e) {
                    }
                    View.display("pong");
                }
            }
        });
        myTread1.start();
        myTread2.start();
        try {
            myTread1.join();
            myTread2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    public void fibonacci(){
        Fibonacci mythread = new Fibonacci(44);
        Fibonacci mythread2 = new Fibonacci(44);
        mythread.start();
        mythread2.start();
        try{
            mythread.join();
            mythread2.join();
        } catch(InterruptedException e){
            e.getStackTrace();
        }
        ExecutorService ex = Executors.newSingleThreadExecutor();
        ex.submit(()->{
            int n1 = 1;
            int n2 = 2;
            int n3 = 0;
            View.display(n1);
            View.display(n2);
            while (n3 < 44) {
                n3 = n1 + n2;
                View.display(n3);
                n1 = n2;
                n2 = n3;
            }
        });
        ex.shutdown();
    }
}
